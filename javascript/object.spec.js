describe("Object", () => {
  it("expect(Object.assign({}, obj).not.toBe(obj))", () => {
    const obj = {}
    expect(Object.assign({}, obj)).not.toBe(obj)
  })

  it("expect(Object.getPrototypeOf(Object.create(myPrototype))).toBe(myPrototype)", () => {
    const myPrototype = { name: 'Noice' }
    expect(Object.getPrototypeOf(Object.create(myPrototype))).toBe(myPrototype)
  })

  it("expect({ fooName: function foo() {} }.fooName.name).toBe('foo')", () => {
    expect({ fooName: function foo() {} }.fooName.name).toBe('foo')
  })

  it("expect({ barName: function () {} }.barName.name).toBe('barName')", () => {
    expect({ barName: function () {} }.barName.name).toBe('barName')
  })

  it("expect({ bazName() {} }.bazName.name).toBe('bazName')", () => {
    expect({ bazName() {} }.bazName.name).toBe('bazName')
  })

  it("expect({ quxName: () => {} }.quxName.name).toBe('quxName')", () => {
    expect({ quxName: () => {} }.quxName.name).toBe('quxName')
  })
})
