describe("spread operator", () => {
  it("expect({ ...obj }).not.toBe(obj)", () => {
    const obj = {}
    expect({ ...obj }).not.toBe(obj)
  })
})
