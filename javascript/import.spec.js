import defaultImport from "./sample/_import"
import * as starAsImport from "./sample/_import"

describe("import", () => {
  it("expect(defaultImport).toBe('defaultValue')", () => {
    expect(defaultImport).toBe("defaultValue")
  })

  it("expect(starAsImport).toEqual({ default: 'defaultValue' })", () => {
    expect(starAsImport).toEqual({ default: 'defaultValue' })
  })
})
