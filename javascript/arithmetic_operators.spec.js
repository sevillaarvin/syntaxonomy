describe("arithmetic operators", () => {
  it("addition:expect(1 + 1).toBe(2)", () => {
    expect(1 + 1).toBe(2)
  })

  it("subtraction:expect(1 - 1).toBe(0)", () => {
    expect(1 - 1).toBe(0)
  })
})
