describe("this", () => {
  describe("object", () => {
    it("expect(myObj.es5()).toBe(myObj)", () => {
      const myObj = {
        es5: function() { return this },
      }

      expect(myObj.es5()).toBe(myObj)
    })

    it("expect(myObj.es6()).toBe(myObj)", () => {
      const myObj = {
        es6: () => this
      }

      expect(myObj.es6()).toBeUndefined()
    })

    it("expect(detached()).toBeUndefined()", () => {
      const myObj = {
        es5: function() { return this }
      }
      const detached = myObj.es5

      expect(detached()).toBeUndefined()
    })
  })

  describe("function", () => {
    it("expect(fnObj.es5()).toBe(fnObj)", () => {
      function FnFactory() {
        this.es5 = function () {
          return this
        }
      }
      const fnObj = new FnFactory()

      expect(fnObj.es5()).toBe(fnObj)
    })

    it("expect(fnDetached()).toBeUndefined()", () => {
      function FnFactory() {
        this.es5 = function () {
          return this
        }
      }
      const fnDetached = (new FnFactory()).es5

      expect(fnDetached()).toBeUndefined()
    })

    it("expect([myObj.es5Passed(), myObj.es6Passed()]).toBe([myObj, undefined])", () => {
      function util(fn) {
        return function () {
          return fn.call(this)
        }
      }
      const myObj = {
        es5Passed: util(function () { return this }),
        es6Passed: util(() => { return this })
      }

      expect([myObj.es5Passed(), myObj.es6Passed()]).toEqual([myObj, undefined])
    })

    it("expect((fn.bind(myObj))()).toBe(myObj)", () => {
      const myObj = { a: 1 }
      const fn = function () { return this }

      expect((fn.bind(myObj))()).toBe(myObj)
    })

    it("expect((fnWithArg.bind(myObj, 1, 2, 3))(4)).toEqual([myObj, 1, 2, 3, 4])", () => {
      const myObj = { a: 1 }
      const fnWithArg = function (a, b, c, d) { return [this, a, b, c, d]}

      expect((fnWithArg.bind(myObj, 1, 2, 3))(4)).toEqual([myObj, 1, 2, 3, 4])
    })

    it("expect(fn.call(myObj, 1, 2, 3)).toEqual([myObj, 1, 2, 3])", () => {
      const myObj = { a: 1 }
      const fn = function (a, b, c) { return [this, a, b, c] }

      expect(fn.call(myObj, 1, 2, 3)).toEqual([myObj, 1, 2, 3])
    })

    it("expect(fn.apply(myObj, [1, 2, 3])).toEqual([myObj, 1, 2, 3])", () => {
      const myObj = { a: 1 }
      const fn = function (a, b, c) { return [this, a, b, c] }

      expect(fn.apply(myObj, [1, 2, 3])).toEqual([myObj, 1, 2, 3])
    })
  })
})
