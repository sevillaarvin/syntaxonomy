describe("Function", () => {
  describe("public", () => {
    it("expect(new Entity('MyItem').item).toBe('MyItem')", () => {
      function Entity(item) {
        this.item = item
      }

      expect(new Entity('MyItem').item).toBe('MyItem')
    })

    it("expect(new Entity().doStuff()).toBe('done')", () => {
      function Entity() {
        const status = 'done'

        this.doStuff = function() {
          return status
        }
      }

      expect(new Entity().doStuff()).toBe('done')
    })

    it("expect(new Entity().protoMethod()).toBe('well-done')", () => {
      function Entity() {}
      Entity.prototype.protoMethod = function() {
        return 'well-done'
      }

      expect(new Entity().protoMethod()).toBe('well-done')
    })

    it("expect(() => { new Entity().protoMethodPrivateData() }).toThrow(ReferenceError)", () => {
      function Entity() {
        const emblem = 'fire'
      }
      Entity.prototype.protoMethodPrivateData = function () {
        return emblem
      }

      expect(() => { new Entity().protoMethodPrivateData() }).toThrow(ReferenceError)
    })
  })

  describe("private", () => {
    it("expect(new Raccoon('Nut').food).toBeUndefined()", () => {
      function Raccoon(item) {
        const food = item
      }

      expect(new Raccoon('Nut').food).toBeUndefined()
    })

    it("expect(() => { new Raccoon().eat() }).toThrow(TypeError)", () => {
      function Raccoon() {
        function eat() {
          return 'yum'
        }
      }

      expect(() => { new Raccoon().eat() }).toThrow(TypeError)
    })
  })

  describe("inheritance", () => {
    it("expect(bInstance.aMethod()).toBe('to a')", () => {
      function AFactory(aProp) {
        this.aProperty = aProp
        this.aMethod = function aMethod() { return this.aProperty }
      }
      function BFactory(aProp, bProp) {
        AFactory.call(this, aProp)
      }
      BFactory.prototype = new AFactory(null)
      const bInstance = new BFactory('to a', 'to b')

      expect(bInstance.aMethod()).toBe('to a')
    })

    it("expect(dInstance.cMethod()).toBe('to c')", () => {
      function CFactory(cProp) {
        this.cProperty = cProp
        this.cMethod = function cMethod() { return this.cProperty }
      }
      function DFactory(cProp) {
        CFactory.call(this, cProp)
      }
      DFactory.prototype = Object.create(CFactory.prototype)
      DFactory.prototype.constructor = DFactory
      const dInstance = new DFactory('to c')

      expect(dInstance.cMethod()).toBe('to c')
    })
  })
})
