#!/bin/bash

function run_language()
{
    local language=$1
    local tag=$2

    case $language in
    javascript*)
        if [ -z $tag ]
        then
            docker-compose run $language
        else
            docker-compose run $language $tag
        fi
        ;;
    ruby)
        if [ -z $tag ]
        then
            docker-compose run $language
        else
            options="TESTOPTS="
            options+="--verbose "
            options+="--name='/$tag/'"
            docker-compose run -e "$options" $language
        fi
        ;;
    php*)
        if [ -z $tag ]
        then
           docker-compose run $language
        else
           docker-compose run $language $tag
        fi
        ;;
    redis)
        if [ -z $tag ]
        then
            docker-compose run $language
        else
            docker-compose run $language $tag
        fi
        ;;
    *sql)
        if [ -z $tag ]
        then
            docker-compose run $language
        else
            docker-compose run $language $tag
        fi
        ;;
    *)
        docker-compose run $language
        ;;
    esac
}

function language_exists()
{
    local language=$1
    egrep "^\s{2}${language}:$" docker-compose.yml
}

function partial_language_exists()
{
    local language=$1
    egrep "^\s{2}${language}.*:$" docker-compose.yml
}

function run_all_languages()
{
    local language=$1
    local languages=($(sed -n -E -e "s/^\s{2}(${language}.*):$/\1/p" docker-compose.yml))
    docker-compose up ${languages[@]}
}

function show_all_languages()
{
    local languages=($(sed -n -E -e "s/^\s{2}([a-zA-Z0-9.-]+):$/\1/p" docker-compose.yml))
    echo '===='
    echo 'Available languages:'
    echo '===='
    printf '* %s\n' "${languages[@]}"
}

function show_usage()
{
    echo 'Usage:'
    echo './syntax.sh $language [$tag]'
    echo ''
    echo '# build everything from scratch'
    echo './syntax.sh clean'
}

function clean_build()
{
    docker-compose down
    docker-compose rm
    docker-compose build
}

language=$1
tag=$2

if [ -z $language ]
then
    show_usage
    echo ''
    show_all_languages
    exit 1
else
    if [ $language = "clean" ]
    then
        clean_build
    elif language_exists $language
    then
        run_language $language $tag
    elif partial_language_exists $language
    then
        run_all_languages $language
    else
        echo "language '$language' not found"
        echo ''
        show_all_languages
        exit 1
    fi
fi
