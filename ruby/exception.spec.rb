require 'minitest/autorun'

describe 'ExceptionTest' do
  def my_function
    raise 'hell'
  end

  it 'raise creates an exception which can be rescue(d)' do
    begin
      my_function
    rescue StandardError => e
      assert_equal 'hell', e.message
    end
  end
end
