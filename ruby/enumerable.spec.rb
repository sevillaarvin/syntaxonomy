require 'minitest/autorun'

describe 'Enumerable' do
  it 'returns partition' do
    list = [1,2,3,4]
    assert_equal list.partition { true }, [[1,2, 3,4], []]
    assert_equal list.partition { |i| i.even? }, [[2,4], [1,3]]
  end
end
