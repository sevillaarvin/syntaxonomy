require 'minitest/autorun'

describe 'Queue' do
  it 'adds item to queue' do
    q = Queue.new
    q << 'item'
    assert_equal q.length, 1
  end

  it 'removes item from queue' do
    q = Queue.new
    q << 'remove'
    result = q.pop
    assert_equal q.length, 0
    assert_equal result, 'remove'
  end

  it 'removes item from another thread' do
    q = Queue.new

    producer = Thread.new do
        q << 'hello'
    end

    result = nil
    consumer = Thread.new do
        result = q.pop
    end

    producer.join
    consumer.join

    assert_equal q.length, 0
    assert_equal result, 'hello'
  end
end
