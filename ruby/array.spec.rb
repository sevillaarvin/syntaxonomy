require 'minitest/autorun'

describe 'ArrayTest' do
  it 'push adds an item to the array' do
    my_array = [1]
    my_array.push(2)
    assert_equal [1, 2], my_array
  end

  it '<< adds an item to the array' do
    my_array = [1]
    my_array << 2
    assert_equal [1, 2], my_array
  end
end
