require 'minitest/autorun'

describe 'CLI' do
  it 'stores arguments to ARGV' do
    assert_equal %w[a b c].inspect, `ruby argv.rb a b c`
  end
end
