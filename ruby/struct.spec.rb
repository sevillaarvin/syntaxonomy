require 'minitest/autorun'

describe 'Struct' do
  it 'creates a value object' do
    the_struct = Struct.new('MyStruct', :prop1, :prop2)
    the_value_object = the_struct.new 'val1', 222
    assert_equal 'val1', the_value_object.prop1
    assert_equal 222, the_value_object.prop2
  end
end
