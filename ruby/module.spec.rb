require 'minitest/autorun'

describe 'Module' do
  module A
    def instance_method
    end
  end

  class B
    include A
  end

  module C
    MODULE_CLASS = (class << self; self; end)

    MODULE_SELF = self

    def self.module_class
      C::MODULE_CLASS
    end

    def self.module_self
      C::MODULE_SELF
    end
  end

  it 'includes instance methods from module' do
    assert_respond_to(B.new, :instance_method)
  end

  it 'returns module eigenclass object' do
    assert_equal C.module_class.class, Class
  end

  it 'returns module object' do
    assert_equal C.module_self.class, Module
  end
end
