require 'minitest/autorun'

describe 'Regexp' do
  it 'matches using case subsumption operator (triple equals)' do
    regexp = /abc/
    assert regexp === 'abc'
    assert !('abc' === regexp)
  end
end
