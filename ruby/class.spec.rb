require 'minitest/autorun'

describe 'ClassTest' do
  class Parent; end
  class Child < Parent; end

  module NewModule
    def hello
      'world'
    end
  end

  class Include
    include NewModule
  end

  class Extend
    extend NewModule
  end

  class InitializeSuper
    def initialize
      @x /= 2.0
      super
    end
  end

  module InitModule
    def initialize(*args)
      @x += 1.0
      super
    end
  end

  class Initialize < InitializeSuper
    include InitModule

    attr_reader :x

    def initialize(*args)
      @x = 0.0
      super
    end
  end

  it 'child is a subclass of parent' do
    assert(Child.new.is_a?(Parent))
  end

  it 'includes object' do
    assert_equal Include.new.hello, 'world'
  end

  it 'extends object' do
    assert_equal Extend.hello, 'world'
  end

  it 'executes initialize' do
    # order of evaluation
    # 1. class itself
    # 2. included module
    # 3. inherited class
    assert_equal Initialize.new.x, 0.5
  end

  describe 'variables' do
    it '@ sets an instance variable' do
      class MyClass
        def initialize
          @myval = 'hello'
        end

        def a_value
          @myval
        end
      end

      assert_equal 'hello', MyClass.new.a_value
    end

    it '@@ sets a class variable' do
      class MyClass
        @@staticval = 'hi'

        def self.a_value
          @@staticval
        end
      end

      assert_equal 'hi', MyClass.a_value
    end

    it 'attr_reader creates a getter method' do
      class MyClass
        attr_reader :my_method

        def initialize
          @my_method = 123
        end
      end

      assert_equal 123, MyClass.new.my_method
    end

    it 'attr_writer creates a setter method' do
      class MyClass
        attr_writer :my_money

        def the_money
          @my_money
        end
      end

      x = MyClass.new
      x.my_money = :xyz
      assert_equal :xyz, x.the_money
    end

    it 'attr_accessor creates getter and setter methods' do
      class MyClass
        attr_accessor :cool_props
      end

      x = MyClass.new
      x.cool_props = 'wow'
      assert_equal 'wow', x.cool_props
    end
  end
end
