require 'minitest/autorun'

describe 'Mutex' do
    describe 'without mutex' do
        it 'is not atomic without mutex' do
            list = [0, 0, 0]

            threads = 5.times.map do
                Thread.new {
                    100000.times do
                        list.map! { |i| i + 1 }
                    end
                }
            end

            threads.each(&:join)
            assert [500000, 500000, 500000] != list
        end

        it 'could return stale values' do
            mutex = Mutex.new
            list = [false, false, false, false, false]
            result = true

            threads = 20.times.map do
                Thread.new {
                    100000.times do
                        result = result && list.all? { |i| list.first == i }

                        mutex.synchronize {
                            list.map! { |b| !b }
                        }
                    end
                }
            end

            threads.each(&:join)
            assert !result
        end
    end

    describe 'with mutex' do
        it 'is atomic with mutex' do
            mutex = Mutex.new
            list = [0, 0, 0]

            threads = 5.times.map do
                Thread.new {
                    100000.times do
                        mutex.synchronize {
                            list.map! { |i| i + 1 }
                        }
                    end
                }
            end

            threads.each(&:join)
            assert_equal [500000, 500000, 500000], list
        end

        it 'always returns fresh values' do
            mutex = Mutex.new
            list = [false, false, false, false, false]
            result = true

            threads = 20.times.map do
                Thread.new {
                    100000.times do
                        mutex.synchronize {
                            result && list.all? { |i| list.first == i }
                        }

                        mutex.synchronize {
                            list.map! { |b| !b }
                        }
                    end
                }
            end

            threads.each(&:join)
            assert result
        end
    end
end
