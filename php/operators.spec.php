<?php

use PHPUnit\Framework\TestCase;

final class OperatorsTest extends TestCase
{
    public function testNullCoalesce()
    {
        $this->assertSame($isUndeclared ?? 123, 123);

        $isUndefined;
        $this->assertSame($isUndefined ?? 456, 456);

        $isNull = null;
        $this->assertSame($isNull ?? 789, 789);

        $isFalse = false;
        $this->assertSame($isFalse ?? 'nein', false);

        $isTrue = true;
        $this->assertSame($isTrue ?? 'lels', true);

        $isZero = 0;
        $this->assertSame($isZero ?? 'haha', 0);

        $isDefined = 'pony';
        $this->assertSame($isDefined ?? 'horse', 'pony');
    }
}
