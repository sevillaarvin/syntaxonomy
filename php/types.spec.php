<?php

use PHPUnit\Framework\TestCase;


try {
    eval('
        function nullableType(?string $string) {
            return $string;
        }
    ');
} catch (ParseError $e) {
    echo $e;
}


final class TypeTest extends TestCase
{
    public function testNullableType()
    {
        $this->assertSame(null, nullableType(null));
        $this->assertSame("john", nullableType("john"));
    }

    # function nullableType(string $string) {
    # function nullableType(?string $string) {
    #     return $string;
    # }
}
