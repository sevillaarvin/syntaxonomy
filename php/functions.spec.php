<?php

use PHPUnit\Framework\TestCase;

final class FunctionsTest extends TestCase
{
    public function testGetClass()
    {
        $this->assertSame(get_class($this), FunctionsTest::class);
    }

    public function testJsonEncode()
    {
        $this->assertSame(json_encode([1,2,3]), "[1,2,3]");
    }
}
