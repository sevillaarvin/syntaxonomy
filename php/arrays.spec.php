<?php

use PHPUnit\Framework\TestCase;

final class ArraysTest extends TestCase
{
    public function testArrayMerge()
    {
        $this->assertSame(array_merge([1,2], [3,4], [5]), [1,2,3,4,5]);
    }
}
