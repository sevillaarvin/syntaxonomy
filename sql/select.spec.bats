#!/usr/bin/env bats

@test "select 1 : 1" {
  result="$(sql 'select 1')"
  [ "$result" = "1" ]
}
