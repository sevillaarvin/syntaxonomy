#!/usr/bin/env bash

# https://stackoverflow.com/a/45364469
psql -U postgres -d postgres -qAtX -c "$@"
