#!/usr/bin/env bash

# start postgresql server
su postgres -c initdb >/dev/null 2>&1
su postgres -c "pg_ctl -D /var/lib/postgresql/data start >/dev/null 2>&1"

set -e

until psql -U postgres -c "select 1" > /dev/null 2>&1
do
  sleep 1
done

exec "bats" "$@"
