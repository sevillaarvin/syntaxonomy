#!/usr/bin/env bash

redis-server >/dev/null &

HOST=127.0.0.1
PORT=6379

PONG=`redis-cli -h $HOST -p $PORT ping 2>/dev/null | grep PONG`
while [ -z "$PONG" ]; do
    sleep 1
    PONG=`redis-cli -h $HOST -p $PORT ping 2>/dev/null | grep PONG`
done

set -e

exec "bats" "$@"
