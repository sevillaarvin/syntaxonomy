#!/usr/bin/env bats

@test "ping : PONG" {
  result="$(redis-cli ping)"
  [ "$result" = "PONG" ]
}
